#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

struct Conexion
{
	double peso;
	double pesoDelta;
};

class Neurona;
typedef vector<Neurona> Capa;


class Neurona
{
public:
	Neurona(int numSalidas, int miIndice);
	void cambiaValorDeSalida(double valor) { m_valorDeSalida = valor;}
	double ObtenerValorDeSalida() const { return m_valorDeSalida;}
	void feedForward(const Capa &capaAnterior);
	void calculaGradientesDeSalida(double targetValues);
	void calculaGradientesOcultos(const Capa &siguienteCapa);
	void actualizaPesosDeEntrada(Capa &capaAnterior);

private:
	double sigmoide(double x);
	double derivadaSigmoide(double x);
	double pesoAleatorio() { return rand() / (double)RAND_MAX;}
	double sumDOW(const Capa &siguienteCapa);
	double m_valorDeSalida;
	vector<Conexion> m_pesosDeSalida;
	int m_miIndice;
	double m_gradient;
	static double eta;
	static double alpha;
};

//tasa de aprendizaje
double Neurona::eta = 0.1;
//momentum
double Neurona::alpha = 0.2;



void Neurona::actualizaPesosDeEntrada(Capa &capaAnterior)
{
	//Los pesos que serán actualizados están en el contenedor Conexion
	// en las neuronas de la capa anterior

	for (int n = 0; n < capaAnterior.size(); ++n)
	{
		Neurona &neurona = capaAnterior[n];
		double pesoDeltaOld = neurona.m_pesosDeSalida[m_miIndice].pesoDelta;
		double nuevoPesoDelta = eta * neurona.ObtenerValorDeSalida() * m_gradient + alpha * pesoDeltaOld;
		neurona.m_pesosDeSalida[m_miIndice].pesoDelta = nuevoPesoDelta;
		neurona.m_pesosDeSalida[m_miIndice].peso += nuevoPesoDelta;
	}

}

double Neurona::sumDOW(const Capa &siguienteCapa)
{
	double suma = 0.0;

	for (int n = 0; n < siguienteCapa.size() - 1; ++n)
	{
		suma += m_pesosDeSalida[n].peso * siguienteCapa[n].m_gradient;
	}
	return suma;
}

void Neurona::calculaGradientesOcultos(const Capa &siguienteCapa)
{
	double dow = sumDOW(siguienteCapa);
	m_gradient = dow * derivadaSigmoide(m_valorDeSalida);
}

void Neurona::calculaGradientesDeSalida(double targetValues)
{
	double delta = targetValues - m_valorDeSalida;
	m_gradient = delta * derivadaSigmoide(m_valorDeSalida);
}


double Neurona::sigmoide(double x)
{
	return 1 / (1 + exp(-x));
}


double Neurona::derivadaSigmoide(double x)
{
	return sigmoide(x) * (1 - sigmoide(x));
}

void Neurona::feedForward(const Capa &capaAnterior) 
{
	double suma = 0.0;

	for (int i = 0; i < capaAnterior.size(); ++i)
	{
		suma += capaAnterior[i].ObtenerValorDeSalida() * 
				capaAnterior[i].m_pesosDeSalida[m_miIndice].peso;
	}
	m_valorDeSalida = sigmoide(suma);
}

Neurona::Neurona(int numSalidas, int miIndice)
{
	for (int i = 0; i < numSalidas; ++i)
	{
		Conexion miConexion;
		m_pesosDeSalida.push_back(miConexion);
		m_pesosDeSalida.back().peso = pesoAleatorio();
	}
	m_miIndice = miIndice;

}

//////////////////////////////////////////////////////////////////////////////////7

class RedNeuronal
{
public:
	RedNeuronal(const vector<int> &topologia);
	void feedForward(const vector<double> &inputValues);
	void backProp(const vector<double> &targetValues);
	void obtenerResultados(vector<double> &vectorDeResultados);
	double obtenerErrorPromedioReciente() {return m_ErrorPromedioReciente;}

private:
	vector<Capa> m_capas; // m_capas[numCapa][numNeurona]
	double m_error;
	double m_ErrorPromedioReciente;
	double m_ErrorPromedioRecienteSmooth;
};


void RedNeuronal::obtenerResultados(vector<double> &vectorDeResultados)
{
	vectorDeResultados.clear();
	for (int n = 0; n < m_capas.back().size() - 1; ++n)
	{
		vectorDeResultados.push_back(m_capas.back()[n].ObtenerValorDeSalida());
	}
}

void RedNeuronal::backProp(const vector<double> &targetValues)
{
	//Calcular todos los errors de la red (RMS)

	Capa &capaDeSalida = m_capas.back();
	m_error = 0.0;
	for (int i = 0; i < capaDeSalida.size() - 1; ++i)
	{
		double delta = targetValues[i] - capaDeSalida[i].ObtenerValorDeSalida();
		m_error += delta * delta;
	}
	m_error /= capaDeSalida.size() - 1;
	m_error = sqrt(m_error);


	m_ErrorPromedioReciente = (m_ErrorPromedioReciente * m_ErrorPromedioRecienteSmooth + m_error)
							/ (m_ErrorPromedioRecienteSmooth +  1.0);
	//calcular los gradientes de las capa de salida

	for (int i = 0; i < capaDeSalida.size() - 1; ++i)
	{
		capaDeSalida[i].calculaGradientesDeSalida(targetValues[i]);
	}

	//Calcular los gradientes de las capas ocultas

	for (int capaNum = m_capas.size() - 2; capaNum > 0; --capaNum)
	{
		Capa &capaOculta = m_capas[capaNum];
		Capa &siguienteCapa = m_capas[capaNum + 1];
		for (int n = 0; n < capaOculta.size(); ++n)
		{
			capaOculta[n].calculaGradientesOcultos(siguienteCapa);	
		}
	}

	//Actualizar los pesos de las conexiones de todos las capas
	//desde las de salida hasta la primer capa oculta

	for (int capaNum = m_capas.size() - 1; capaNum > 0; --capaNum)
	{
		Capa &capa = m_capas[capaNum];
		Capa &capaAnterior = m_capas[capaNum - 1];

		for (int n = 0; n < capa.size() - 1; ++n)
		{
			capa[n].actualizaPesosDeEntrada(capaAnterior);
		}
	}


}

void RedNeuronal::feedForward(const vector<double> &inputValues)
{
	assert(inputValues.size() == m_capas[0].size() - 1);

	for (int i = 0; i < inputValues.size(); ++i)
	{
		m_capas[0][i].cambiaValorDeSalida(inputValues[i]);
	}
	for (int capaNum = 1; capaNum < m_capas.size(); ++capaNum)
	{
		Capa &capaAnterior = m_capas[capaNum -  1];
		for (int n = 0; n < m_capas[capaNum].size() - 1; ++n)
		{
			m_capas[capaNum][n].feedForward(capaAnterior);
		}
	}

}


RedNeuronal::RedNeuronal(const vector<int> &topologia) 
{
	int numCapas = topologia.size();
	for (int capaNum = 0; capaNum < numCapas; ++capaNum)
	{	
		int numSalidas = capaNum == topologia.size() - 1 ? 0 : topologia[capaNum + 1]; 

		m_capas.push_back(Capa());
		for (int neuronaNum = 0; neuronaNum <= topologia[capaNum]; ++neuronaNum)
		{
			m_capas.back().push_back(Neurona(numSalidas, neuronaNum));
		}

		m_capas.back().back().cambiaValorDeSalida(1.0);

	}
}


///////////////////////////////////////////////////////////7
class TrainingSet
{
public:
	TrainingSet(string nombreArchivo);
	int obtenerTargetValues(vector<double> &valoresTarget, int index);
	int numeroDeUnidadesDeEntrada() { return (int)Tuplas[0].size(); }
	int sizeOf() { return (int)Tuplas.size();}

	vector<vector<double>> Tuplas;
};

int TrainingSet::obtenerTargetValues(vector<double> &valoresTarget, int index)
{
	valoresTarget.clear();
	valoresTarget.push_back(Tuplas[index].back()); 
	return (int)valoresTarget.size();
}
TrainingSet::TrainingSet(string nombreArchivo)
{
	ifstream archivoCSV (nombreArchivo);
	string tupla;
	vector<double> fila;
	bool header = true;

	while (getline(archivoCSV, tupla)) {
		stringstream lineStream(tupla);
		string celda;
		fila.clear();
		while (getline(lineStream, celda, ';')) 
			if (!header) fila.push_back(stod(celda));
		if(!header) Tuplas.push_back(fila);	
		header = false;
	}
}


void imprimeVector (const vector<double> &v) 
{
	for (int i = 0; i < v.size(); ++i)
	{
		cout << v[i] << " ";
	}
	cout << endl;
}

class MatrizDeConfusion
{
public:
	MatrizDeConfusion(){ truePositives = 0; trueNegatives = 0; falsePositives = 0; falseNegatives = 0; totalPositivos = 0; totalNegativos = 0;};
	void addValues(const double &resultado, const double &real);
	double calcularAccuracy();
private:
	int truePositives;
	int trueNegatives;
	int falsePositives;
	int falseNegatives;
	int totalPositivos;
	int totalNegativos;
	int total;
};



void MatrizDeConfusion::addValues(const double &resultado, const double &real)
{
	if (fabs(round(resultado) - real) <= 1e-4)
	{

		if (round(real) == 1){
			truePositives++;
			totalPositivos++;

		}
		else{
			trueNegatives++;
			totalNegativos++;

		}
	}
	else
	{

		if (round(real) == 1){
			falseNegatives++;
			totalPositivos++;

		}
		else {
			falsePositives++;
			totalNegativos++;

		}
	}
}

double MatrizDeConfusion::calcularAccuracy()
{

	cout << "TP " << truePositives << endl;
	cout << "TN " << trueNegatives << endl;
	cout << "FP " << falsePositives << endl;
	cout << "FN " << falseNegatives << endl;
	total = totalNegativos + totalPositivos;	
	double sensitivity = (double) truePositives / (double) (truePositives + falseNegatives); 
	double Specificity = (double) trueNegatives / (double) (trueNegatives + falsePositives);
	double Precision = (double) truePositives / (double) (truePositives + falsePositives);
	double F1Score = (Precision  * sensitivity) / (Precision + sensitivity);
	
	double MCC = truePositives * trueNegatives - ((falsePositives * falseNegatives) / sqrt((double)(truePositives + falsePositives) * (double)(truePositives + falseNegatives) * (double)(trueNegatives + falsePositives) * (double)(trueNegatives + falseNegatives)));


	cout << "Sensitivity " << endl;
	cout << sensitivity << endl;
	cout << "Specificity" << endl;
	cout << Specificity << endl;
	cout << "Precision" << endl;
	cout << Precision << endl;
	cout << "F1Score" << endl;
	cout << F1Score << endl;

	cout << "Accuracy" << endl;
	return ((double)trueNegatives + (double)truePositives) / ((double)(truePositives + trueNegatives + falsePositives + falseNegatives));
}

int main(int argc, char const *argv[])
{

	TrainingSet conjuntoDeEntrenamiento("trainData.csv");
	TrainingSet conjuntoDePrueba("testData.csv");

	MatrizDeConfusion Performance;

	vector<int> topologia;
	int numeroDeCapasOcultas;
	int numeroDeUnidades;
	int numeroDeUnidadesCapaDeSalida;

	cin >> numeroDeCapasOcultas >> numeroDeUnidadesCapaDeSalida;

	topologia.push_back(conjuntoDeEntrenamiento.numeroDeUnidadesDeEntrada());

	for (int i = 0; i < numeroDeCapasOcultas; ++i)
	{
		cin >> numeroDeUnidades;
		topologia.push_back(numeroDeUnidades);
	}

	topologia.push_back(numeroDeUnidadesCapaDeSalida);


	RedNeuronal Red(topologia);

	vector<double> resultados;
	vector<double> valoresTarget;
		
	int iterador = 0;

	for (int numTupla = 0; ; numTupla = (numTupla + 1) % conjuntoDeEntrenamiento.sizeOf())
	{
		if (iterador++ > 800)
		{
			break;
		}

		
		Red.feedForward(conjuntoDeEntrenamiento.Tuplas[numTupla]);

		Red.obtenerResultados(resultados);

	
		conjuntoDeEntrenamiento.obtenerTargetValues(valoresTarget, numTupla);
		Red.backProp(valoresTarget);

	}
	for (int numTupla = 0; numTupla < conjuntoDePrueba.sizeOf(); ++numTupla)
	{

		
		Red.feedForward(conjuntoDePrueba.Tuplas[numTupla]);

		Red.obtenerResultados(resultados);


	
		conjuntoDePrueba.obtenerTargetValues(valoresTarget, numTupla);
		Red.backProp(valoresTarget);
		
		Performance.addValues(resultados.back(), valoresTarget.back());

	}
	cout << Performance.calcularAccuracy() << endl;
	
	return 0;
}