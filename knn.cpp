#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
using namespace std;


typedef vector<double> tupla;

bool ordenaPorDistacia(tupla a, tupla b) {
	return a.back() < b.back();
}

class Knn
{
public:
	Knn(){};
	double distanciaEuclidiana (const tupla &x1, const tupla &x2);
	void obtenerVecinos(vector<tupla> conjuntoDeEntrenamiento, tupla muestraDesconocida, const int &k);
	double etiquetarMuestra(tupla &muestraDesconocidaKNN);
private:
	vector<tupla> vecinos;
};

void Knn::obtenerVecinos(vector<tupla> conjuntoDeEntrenamiento, tupla muestraDesconocida, const int &k)
{
	vecinos.clear();
	for (int i = 0; i < (int) conjuntoDeEntrenamiento.size(); ++i)
	{
		tupla &tuplaEtiquetada = conjuntoDeEntrenamiento[i];
		tuplaEtiquetada.push_back(distanciaEuclidiana(tuplaEtiquetada, muestraDesconocida));
	}
	sort(conjuntoDeEntrenamiento.begin(), conjuntoDeEntrenamiento.end(), ordenaPorDistacia);

	
	for (int i = 0; i < k; ++i)
	{
		vecinos.push_back(conjuntoDeEntrenamiento[i]);
	}
	
}

double Knn::etiquetarMuestra(tupla &muestraDesconocidaKNN)
{
	//1's
	int elementosTipo1 = 0;
	//0's
	int elementosTipo2 = 0;
	for (int i = 0; i < (int) vecinos.size(); ++i)
	{
		round(vecinos[i][vecinos[i].size() - 2]) == 1 ? elementosTipo1++ : elementosTipo2++;
	}
	elementosTipo1 > elementosTipo2 ? muestraDesconocidaKNN.push_back(1.0) : muestraDesconocidaKNN.push_back(0.0);
	return muestraDesconocidaKNN.back();
}


double Knn::distanciaEuclidiana (const tupla &x1, const tupla &x2)
{
	double distancia = 0.0;
	for (int i = 0; i < (int) x1.size(); ++i)
		distancia += (x1[i] - x2[i]) * (x1[i] - x2[i]);
	return (distancia);
}



class TrainingSet
{
public:
	TrainingSet(string nombreArchivo, bool header);
	int sizeOf() { return (int)Tuplas.size();}
	vector<vector<double>> Tuplas;

};

TrainingSet::TrainingSet(string nombreArchivo, bool header)
{
	ifstream archivoCSV (nombreArchivo);
	string tupla;
	vector<double> fila;

	while (getline(archivoCSV, tupla)) {
		stringstream lineStream(tupla);
		string celda;
		fila.clear();
		while (getline(lineStream, celda, ';')) 
			if (!header) fila.push_back(stod(celda));
		if(!header) Tuplas.push_back(fila);	
		header = false;
	}
}


void imprimeVector (const vector<double> &v) 
{
	for (int i = 0; i < (int) v.size(); ++i)
		cout << v[i] << " ";
	cout << endl;
}


class MatrizDeConfusion
{
public:
	MatrizDeConfusion(){ truePositives = 0; trueNegatives = 0; falsePositives = 0; falseNegatives = 0; totalPositivos = 0; totalNegativos = 0;};
	void addValues(const double &resultado, const double &real);
	double calcularAccuracy();
private:
	long long truePositives;
	long long trueNegatives;
	long long falsePositives;
	long long falseNegatives;
	long long totalPositivos;
	long long totalNegativos;
	long long total;
};



void MatrizDeConfusion::addValues(const double &resultado, const double &real)
{
	if (fabs(round(resultado) - real) <= 1e-4)
		round(real) == 1 ? truePositives++, totalPositivos++ : trueNegatives++, totalNegativos++;
	else
		round(real) == 1 ?  falseNegatives++, totalPositivos++ : falsePositives++, totalNegativos++;
}

double MatrizDeConfusion::calcularAccuracy()
{

	cout << "TP " << truePositives << endl;
	cout << "TN " << trueNegatives << endl;
	cout << "FP " << falsePositives << endl;
	cout << "FN " << falseNegatives << endl;
	total = totalNegativos + totalPositivos;	
	double sensitivity = (double) truePositives / (double) (truePositives + falseNegatives); 
	double Specificity = (double) trueNegatives / (double) (trueNegatives + falsePositives);
	double Precision = (double) truePositives / (double) (truePositives + falsePositives);
	double F1Score = (Precision  * sensitivity) / (Precision + sensitivity);
	
	
	cout << "Sensitivity " << endl;
	cout << sensitivity << endl;
	cout << "Specificity" << endl;
	cout << Specificity << endl;
	cout << "Precision" << endl;
	cout << Precision << endl;
	//cout << "F1Score" << endl;
	//cout << F1Score << endl;
	cout << "Accuracy" << endl;
	return ((double)trueNegatives + (double)truePositives) / ((double)(truePositives + trueNegatives + falsePositives + falseNegatives));
}


int main(int argc, char const *argv[])
{
	int kfactor = 0;
	cin >> kfactor;
	TrainingSet conjuntoDePrueba("test.txt", true);	
	TrainingSet conjuntoDeEntrenamiento("train.txt", true);
	MatrizDeConfusion Performance;
	Knn AlgoritmoKNN;
	double resultado, real;
	for (int i = 0; i < (int) conjuntoDePrueba.sizeOf(); ++i)
	{
		AlgoritmoKNN.obtenerVecinos(conjuntoDeEntrenamiento.Tuplas, conjuntoDePrueba.Tuplas[i], kfactor);
		AlgoritmoKNN.etiquetarMuestra(conjuntoDePrueba.Tuplas[i]);
		resultado = conjuntoDePrueba.Tuplas[i].back();
		real = conjuntoDePrueba.Tuplas[i][conjuntoDePrueba.Tuplas[i].size() - 2];
		cout << "Valor real = " << real << endl;
		cout << "Prediccion = " << resultado << endl;
		Performance.addValues(resultado, real);
	}	

	cout << Performance.calcularAccuracy() << endl;

	return 0;
}