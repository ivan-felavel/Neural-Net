Red neuronal
===================

# Descripción
--------
Una red neuronal FF (feed forward) en **C++** y usando la **STL**. Utiliza el algoritmo de retropropagación para el aprendizaje supervisado y funciona con archivos de texto separados por **";"**. 

### Características del código
El diseño es orientado a objetos, tenemos la clase ```RedNeuronal``` cuya estructura principal es un atributo ```m_capas```, que se puede ver como un arreglo de instancias de la clase ```Neurona```.

Además de esas dos clases tenemos otras dos, la clase ```TrainingSet```, que tiene como función la lectura del archivo de texto con el conjunto de datos y la clase ```MatrizDeConfusion``` que sirve para evaluar el desempeño de nuestro clasificador.

### Entrada del programa

El programa recibe como datos de entrada la topología de la red, esto es:

* Número de capas ocultas
* Número de unidades de salida
* Número de unidades en cada capa de salida

Un ejemplo de una entrada válida es el siguiente:
```
2 1
13
13
```
El primer renglón significa que nuestra red neuronal tiene dos capas ocultas y que solo hay una unidad de salida, las dos líneas siguientes indican que en la primera capa oculta hay 13 unidades la igual que en la segunda. 

### Lectura del conjunto de datos

El clasificador trabaja con dos archivos de texto, uno para el entrenamiento, y el otro para la prueba. Cada uno de los dos mencionandos debe ser un archivo de texto separado por ";". Los atributos deben ser numéricos, por lo que es necesario normalizar el conjunto de datos y cabe señalar que la variable objetivo debe estar en la última columna del archivo. 
Es fácil de usar con cualquier conjunto de datos en un archivo de texto, solo es necesario cambiar los parámetros del constructor de la clase ```TrainingSet```, para que quede de la siguiente forma:
```c++
TrainingSet NombreDeMiInstancia("mi archivo de texto separado por ;");
```
### Compilando y corriendo
---------
Para compilar el programa lo hacemos de la siguiente forma:
```sh
g++ --std=c++11 neural_net.cpp -o net
```
Y para correr
```sh
./net < topologia.in 
```
El ```main``` se encarga de crear la instancias necesarias de cada clase, hace 800 iteraciones para el entrenamiento, y después evalúa las nuevas tuplas del archivo de prueba, para finalmente mostrar la salida del programa.

### Salida del programa
La salida del programa solo son los resultados de la evaluación del desempeño del programa, usando los datos que vienen por defecto la salida es:

```
TP 104
TN 112
FP 0
FN 19
Sensitivity 
0.845528
Specificity
1
Precision
1
F1Score
0.45815
Accuracy
0.919149
```
### Código Fuente
----------
https://gitlab.com/ivan-felavel/Neural-Net/



